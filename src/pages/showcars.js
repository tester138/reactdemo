import { useState, useEffect } from 'react'
import axios from 'axios'
import { toast } from 'react-toastify'

const Showcars = () => {
  const [carList, setCarList] = useState([])

  useEffect(() => {
    axios
      .get('http://localhost:4000/car/showcars')
      .then((response) => {
        const result = response.data
        console.log(result)
        setCarList(result.data)
        toast.success('Cars fetched successfully')
      })
      .catch((error) => {
        console.log(error)
      })
  }, [])

  return (
    <div className='container'>
      <h1>Show Cars</h1>

      {carList.map((props) => {
        return (
          <div
            key={props.id}
            className='card'
            style={{
              height: 400,
              width: 380,
              margin: 20,
              display: 'inline-block',
            }}>
            <img
              src={`${props.carImage}`}
              class='card-img-top'
              alt='carImage'></img>
            <div className='card-body'>
              <h4 className='card-title'>{props.companyName}</h4>
              <h4 className='card-title'>{props.model}</h4>
              <h4 className='card-title'>{props.price}Rs.</h4>
              {/* <h4 className='card-title'>{props.carImage}</h4> */}
              <p className='card-text'>
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
            </div>
          </div>
        )
      })}
    </div>
  )
}

export default Showcars
