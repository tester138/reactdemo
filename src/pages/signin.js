import { useState } from 'react'
import axios from 'axios'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router-dom'

const Signin = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const navigate = useNavigate()

  const signin = () => {
    // check if user has really entered any value
    if (email.length === 0) {
      toast.error('please enter email')
    } else if (password.length === 0) {
      toast.error('please enter password')
    } else {
      axios
        .post('http://localhost:4000/user/signin', {
          email,
          password,
        })
        .then((response) => {
          const result = response.data
          if (result['status'] === 'error') {
            toast.error('invalid email or password')
          } else {
            toast.success('welcome to Showroom')
            navigate('/showcars')
          }
        })
    }
  }

  return (
    <div
      style={{
        width: 500,
        height: 300,
        position: 'relative',
        top: 100,
        left: 0,
        bottom: 0,
        margin: 'auto',
        borderStyle: 'solid',
        borderColor: 'lightblue',
        borderRadius: 20,
        padding: 30,
      }}>
      <div className='mb-3'>
        <label className='form-label'>Email</label>
        <input
          type='email'
          className='form-control'
          onChange={(event) => {
            setEmail(event.target.value)
          }}
        />
      </div>
      <div className='mb-3'>
        <label className='form-label'>Password</label>
        <input
          type='password'
          className='form-control'
          onChange={(event) => {
            setPassword(event.target.value)
          }}
        />
      </div>
      <div className='mb-3'>
        <button onClick={signin} className='btn btn-success'>
          Signin
        </button>
      </div>
    </div>
  )
}

export default Signin
