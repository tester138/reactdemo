import { useState } from 'react'
import axios from 'axios'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router-dom'

const Addcar = () => {
  const [id, setId] = useState(0)
  const [companyName, setCompanyName] = useState('')
  const [model, setModel] = useState('')
  const [price, setPrice] = useState(0)
  const [carImage, setCarImage] = useState('')

  const navigate = useNavigate()

  const addcar = () => {
    // check if user has really entered any value
    if (id.length === 0) {
      toast('please enter id')
    } else if (companyName.length === 0) {
      toast('please enter companyName')
    } else if (model.length === 0) {
      toast('please enter model')
    } else if (price.length === 0) {
      toast('please enter price')
    } else if (carImage.length === 0) {
      toast('please enter carImage')
    } else {
      axios
        .post('http://localhost:4000/car/add', {
          id,
          companyName,
          model,
          price,
          carImage,
        })
        .then((response) => {
          const result = response.data
          if (result['status'] === 'error') {
            toast.error('Error: car adding failed')
          } else {
            toast.success('Car Added Successfully')

            navigate('/showcars')
          }
        })
    }
  }

  return (
    <div
      style={{
        width: 500,
        height: 600,
        position: 'relative',
        top: 100,
        left: 0,
        bottom: 0,
        margin: 'auto',
        borderStyle: 'solid',
        borderColor: 'lightblue',
        borderRadius: 20,
        padding: 30,
      }}>
      <div className='mb-3'>
        <label className='form-label'>Car-id</label>
        <input
          type='number'
          className='form-control'
          onChange={(event) => {
            setId(event.target.value)
          }}
        />
      </div>
      <div className='mb-3'>
        <label className='form-label'>Company Name</label>
        <input
          type='text'
          className='form-control'
          onChange={(event) => {
            setCompanyName(event.target.value)
          }}
        />
      </div>
      <div className='mb-3'>
        <label className='form-label'>Model</label>
        <input
          type='text'
          className='form-control'
          onChange={(event) => {
            setModel(event.target.value)
          }}
        />
      </div>
      <div className='mb-3'>
        <label className='form-label'>Price</label>
        <input
          type='text'
          className='form-control'
          onChange={(event) => {
            setPrice(event.target.value)
          }}
        />
      </div>
      <div className='mb-3'>
        <label className='form-label'>Image</label>
        <input
          type='file'
          className='form-control'
          onChange={(event) => {
            setCarImage(event.target.value)
          }}
        />
      </div>
      <div className='mb-3'>
        <button onClick={addcar} className='btn btn-success'>
          Add car
        </button>
      </div>
    </div>
  )
}

export default Addcar
