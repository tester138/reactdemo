import { useState } from 'react'
import axios from 'axios'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router-dom'

const Deletecar = () => {
  const [id, setId] = useState('')

  const navigate = useNavigate()

  const deletecar = () => {
    // check if user has really entered any value
    if (id.length === 0) {
      toast('please enter id')
    } else {
      axios.delete(`http://localhost:4000/car/${id}`).then((response) => {
        const result = response.data
        if (result['status'] === 'error') {
          toast.error('Error: car deleting failed')
        } else {
          toast.success('Car Deleted Successfully')

          navigate('/showcars')
        }
      })
    }
  }

  return (
    <div
      style={{
        width: 500,
        height: 300,
        position: 'relative',
        top: 100,
        left: 0,
        bottom: 0,
        margin: 'auto',
        borderStyle: 'solid',
        borderColor: 'lightblue',
        borderRadius: 20,
        padding: 30,
      }}>
      <div className='mb-3'>
        <label className='form-label'>Car-id</label>
        <input
          type='number'
          className='form-control'
          onChange={(event) => {
            setId(event.target.value)
          }}
        />
      </div>

      <div className='mb-3'>
        <button onClick={deletecar} className='btn btn-success'>
          Delete car
        </button>
      </div>
    </div>
  )
}

export default Deletecar
