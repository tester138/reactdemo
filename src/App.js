import Addcar from './pages/addcar'
import Deletecar from './pages/deletecar'
import Showcars from './pages/showcars'
import Signin from './pages/signin'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Navbar from './components/navbar'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path='/signin' element={<Signin />} />
        <Route path='/addcar' element={<Addcar />} />
        <Route path='/showcars' element={<Showcars />} />
        <Route path='/deletecar' element={<Deletecar />} />
      </Routes>
      {/* this container is used to show toast messages */}
      <ToastContainer position='top-center' autoClose={1000} />
    </BrowserRouter>
  )
}

export default App
