# public/index.html

```js
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <link rel="icon" href="%PUBLIC_URL%/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <meta
      name="description"
      content="Web site created using create-react-app"
    />
    <link rel="apple-touch-icon" href="%PUBLIC_URL%/logo192.png" />
    <!--
      manifest.json provides metadata used when your web app is installed on a
      user's mobile device or desktop. See https://developers.google.com/web/fundamentals/web-app-manifest/
    -->
    <link rel="manifest" href="%PUBLIC_URL%/manifest.json" />
    <!--
      Notice the use of %PUBLIC_URL% in the tags above.
      It will be replaced with the URL of the `public` folder during the build.
      Only files inside the `public` folder can be referenced from the HTML.

      Unlike "/favicon.ico" or "favicon.ico", "%PUBLIC_URL%/favicon.ico" will
      work correctly both with client-side routing and a non-root public URL.
      Learn how to configure a non-root public URL by running `npm run build`.
    -->
    <title>Showroom App</title>
    <!-- CSS only -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
      crossorigin="anonymous"
    />
  </head>
  <body>
    <div class="container">
      <noscript>You need to enable JavaScript to run this app.</noscript>
      <div id="root"></div>
    </div>
    <!--
      This HTML file is a template.
      If you open it directly in the browser, you will see an empty page.

      You can add webfonts, meta tags, or analytics to this file.
      The build step will place the bundled scripts into the <body> tag.

      To begin the development, run `npm start` or `yarn start`.
      To create a production bundle, use `npm run build` or `yarn build`.
    -->
  </body>
</html>

```

# src/App.js

```js
import Addcar from './pages/addcar'
import Deletecar from './pages/deletecar'
import Showcars from './pages/showcars'
import Signin from './pages/signin'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Navbar from './components/navbar'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path='/signin' element={<Signin />} />
        <Route path='/addcar' element={<Addcar />} />
        <Route path='/showcars' element={<Showcars />} />
        <Route path='/deletecar' element={<Deletecar />} />
      </Routes>
      {/* this container is used to show toast messages */}
      <ToastContainer position='top-center' autoClose={1000} />
    </BrowserRouter>
  )
}

export default App
```

# src/pages/addcar.js

```js
import { useState } from 'react'
import axios from 'axios'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router-dom'

const Addcar = () => {
  const [id, setId] = useState(0)
  const [companyName, setCompanyName] = useState('')
  const [model, setModel] = useState('')
  const [price, setPrice] = useState(0)
  const [carImage, setCarImage] = useState('')

  const navigate = useNavigate()

  const addcar = () => {
    // check if user has really entered any value
    if (id.length === 0) {
      toast('please enter id')
    } else if (companyName.length === 0) {
      toast('please enter companyName')
    } else if (model.length === 0) {
      toast('please enter model')
    } else if (price.length === 0) {
      toast('please enter price')
    } else if (carImage.length === 0) {
      toast('please enter carImage')
    } else {
      axios
        .post('http://localhost:4000/car/add', {
          id,
          companyName,
          model,
          price,
          carImage,
        })
        .then((response) => {
          const result = response.data
          if (result['status'] === 'error') {
            toast.error('Error: car adding failed')
          } else {
            toast.success('Car Added Successfully')

            navigate('/showcars')
          }
        })
    }
  }

  return (
    <div
      style={{
        width: 500,
        height: 600,
        position: 'relative',
        top: 100,
        left: 0,
        bottom: 0,
        margin: 'auto',
        borderStyle: 'solid',
        borderColor: 'lightblue',
        borderRadius: 20,
        padding: 30,
      }}>
      <div className='mb-3'>
        <label className='form-label'>Car-id</label>
        <input
          type='number'
          className='form-control'
          onChange={(event) => {
            setId(event.target.value)
          }}
        />
      </div>
      <div className='mb-3'>
        <label className='form-label'>Company Name</label>
        <input
          type='text'
          className='form-control'
          onChange={(event) => {
            setCompanyName(event.target.value)
          }}
        />
      </div>
      <div className='mb-3'>
        <label className='form-label'>Model</label>
        <input
          type='text'
          className='form-control'
          onChange={(event) => {
            setModel(event.target.value)
          }}
        />
      </div>
      <div className='mb-3'>
        <label className='form-label'>Price</label>
        <input
          type='text'
          className='form-control'
          onChange={(event) => {
            setPrice(event.target.value)
          }}
        />
      </div>
      <div className='mb-3'>
        <label className='form-label'>Image</label>
        <input
          type='file'
          className='form-control'
          onChange={(event) => {
            setCarImage(event.target.value)
          }}
        />
      </div>
      <div className='mb-3'>
        <button onClick={addcar} className='btn btn-success'>
          Add car
        </button>
      </div>
    </div>
  )
}

export default Addcar
```

# src/pages/deletecar.js

```js
import { useState } from 'react'
import axios from 'axios'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router-dom'

const Deletecar = () => {
  const [id, setId] = useState('')

  const navigate = useNavigate()

  const deletecar = () => {
    // check if user has really entered any value
    if (id.length === 0) {
      toast('please enter id')
    } else {
      axios.delete(`http://localhost:4000/car/${id}`).then((response) => {
        const result = response.data
        if (result['status'] === 'error') {
          toast.error('Error: car deleting failed')
        } else {
          toast.success('Car Deleted Successfully')

          navigate('/showcars')
        }
      })
    }
  }

  return (
    <div
      style={{
        width: 500,
        height: 300,
        position: 'relative',
        top: 100,
        left: 0,
        bottom: 0,
        margin: 'auto',
        borderStyle: 'solid',
        borderColor: 'lightblue',
        borderRadius: 20,
        padding: 30,
      }}>
      <div className='mb-3'>
        <label className='form-label'>Car-id</label>
        <input
          type='number'
          className='form-control'
          onChange={(event) => {
            setId(event.target.value)
          }}
        />
      </div>

      <div className='mb-3'>
        <button onClick={deletecar} className='btn btn-success'>
          Delete car
        </button>
      </div>
    </div>
  )
}

export default Deletecar
```

# src/pages/showcars.js

```js
import { useState, useEffect } from 'react'
import axios from 'axios'
import { toast } from 'react-toastify'

const Showcars = () => {
  const [carList, setCarList] = useState([])

  useEffect(() => {
    axios
      .get('http://localhost:4000/car/showcars')
      .then((response) => {
        const result = response.data
        console.log(result)
        setCarList(result.data)
        toast.success('Cars fetched successfully')
      })
      .catch((error) => {
        console.log(error)
      })
  }, [])

  return (
    <div className='container'>
      <h1>Show Cars</h1>

      {carList.map((props) => {
        return (
          <div
            key={props.id}
            className='card'
            style={{
              height: 400,
              width: 380,
              margin: 20,
              display: 'inline-block',
            }}>
            <img
              src={`${props.carImage}`}
              class='card-img-top'
              alt='carImage'></img>
            <div className='card-body'>
              <h4 className='card-title'>{props.companyName}</h4>
              <h4 className='card-title'>{props.model}</h4>
              <h4 className='card-title'>{props.price}Rs.</h4>
              {/* <h4 className='card-title'>{props.carImage}</h4> */}
              <p className='card-text'>
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
            </div>
          </div>
        )
      })}
    </div>
  )
}

export default Showcars
```

# src/pages/signin.js

```js
import { useState } from 'react'
import axios from 'axios'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router-dom'

const Signin = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const navigate = useNavigate()

  const signin = () => {
    // check if user has really entered any value
    if (email.length === 0) {
      toast.error('please enter email')
    } else if (password.length === 0) {
      toast.error('please enter password')
    } else {
      axios
        .post('http://localhost:4000/user/signin', {
          email,
          password,
        })
        .then((response) => {
          const result = response.data
          if (result['status'] === 'error') {
            toast.error('invalid email or password')
          } else {
            toast.success('welcome to Showroom')
            navigate('/showcars')
          }
        })
    }
  }

  return (
    <div
      style={{
        width: 500,
        height: 300,
        position: 'relative',
        top: 100,
        left: 0,
        bottom: 0,
        margin: 'auto',
        borderStyle: 'solid',
        borderColor: 'lightblue',
        borderRadius: 20,
        padding: 30,
      }}>
      <div className='mb-3'>
        <label className='form-label'>Email</label>
        <input
          type='email'
          className='form-control'
          onChange={(event) => {
            setEmail(event.target.value)
          }}
        />
      </div>
      <div className='mb-3'>
        <label className='form-label'>Password</label>
        <input
          type='password'
          className='form-control'
          onChange={(event) => {
            setPassword(event.target.value)
          }}
        />
      </div>
      <div className='mb-3'>
        <button onClick={signin} className='btn btn-success'>
          Signin
        </button>
      </div>
    </div>
  )
}

export default Signin
```

# src/components/navbar.js

```js
import { Link } from 'react-router-dom'

const Navbar = () => {
  return (
    <nav className='navbar navbar-expand-lg navbar-dark bg-primary'>
      <div className='container-fluid'>
        <Link className='navbar-brand' to='/showcars'>
          ShowRoom
        </Link>

        <button
          className='navbar-toggler'
          type='button'
          data-bs-toggle='collapse'
          data-bs-target='#navbarSupportedContent'
          aria-controls='navbarSupportedContent'
          aria-expanded='false'
          aria-label='Toggle navigation'>
          <span className='navbar-toggler-icon'></span>
        </button>

        <div className='collapse navbar-collapse'>
          <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
            <li className='nav-item'>
              <Link
                className='nav-link active'
                aria-current='page'
                to='/showcars'>
                Show Car
              </Link>
            </li>
            <li className='nav-item'>
              <Link
                className='nav-link active'
                aria-current='page'
                to='/addcar'>
                AddCar
              </Link>
            </li>
            <li className='nav-item'>
              <Link
                className='nav-link active'
                aria-current='page'
                to='/deletecar'>
                DeleteCar
              </Link>
            </li>
            <li className='nav-item'>
              <Link
                className='nav-link active'
                aria-current='page'
                to='/signin'>
                Signin
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Navbar
```
